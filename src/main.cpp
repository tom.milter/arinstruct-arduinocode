#include <Arduino.h>
#include <FastLED.h>
#include <math.h>

//1. Button  mit PULLUP ( Pin 11)
//2. LED     mit Widerstand ( Pin 7)
//3. RGB LED          (Pin 2+3+4 + GND)
//4. Chip LED         (Pin 22?)
//5. Widerstand messen  (A8)
//6. Photosensor        (selber wie oben ?)
//7. Schieberegister    (LEDs +  4Pins ?)


#define BUTTON_PIN 11
bool ButtonPressed = false;
#define LED_PIN     7
bool LEDConnected = false;
#define R_PIN       4
#define G_PIN       3
#define B_PIN       2
bool RGBConnected = false;

#define Do_PIN      22

#define RES_PIN     A8

#define Register5V 38
#define MasterReset 22
#define ShiftClock  26
#define StorageClock 30
#define Data        34

bool ShiftRegisterConnected = false;
int RegisterStep = 0;
int RegisterSmallStep = 0;

bool li = false;

// #define Wiederstand



void setup() {

  Serial.begin(9600);

  pinMode(BUTTON_PIN,INPUT_PULLUP);
  pinMode(LED_PIN,INPUT_PULLUP);
  pinMode(R_PIN,INPUT_PULLUP);
  pinMode(G_PIN,INPUT_PULLUP);
  pinMode(B_PIN,INPUT_PULLUP);
  
  pinMode(Do_PIN,OUTPUT);

  pinMode(RES_PIN,INPUT);

  pinMode(Register5V, INPUT_PULLUP); //zum Detecten des Anschlusses;
  pinMode(MasterReset,OUTPUT);
  pinMode(ShiftClock,OUTPUT);
  pinMode(StorageClock,OUTPUT);
  pinMode(Data,OUTPUT);

  delay(5000); // Delay des Python-Codes am Raspi ausgleichen

}

void WriteData(bool datenbyte[7]) {

  for (int i = 0; i < 8 ; i++) {

    digitalWrite(Data, datenbyte[i]);
    digitalWrite(ShiftClock, HIGH);
    delay(1);
    digitalWrite(ShiftClock,LOW);
    delay(1);

  }
    
}

void StoreDataRegister() {

  digitalWrite(StorageClock, HIGH);
  delay(1);
  digitalWrite(StorageClock, LOW);

}

void Schieberegister() {

  static uint32_t lastTimeRegister;
  static bool l = false;
  if (ShiftRegisterConnected == true) {

    if (millis() - lastTimeRegister > 1000) {

      if (RegisterStep == 0) {
        String o = String("RegisterStep " + String(RegisterStep));
        Serial.print(o);

        bool li[] = {false, false,false,false,false,false,false,true};       //falschherrum, da der Write() befehl von vorne nach hinten durchschiebt und somit der letzte eintrag als letztes geschrieben wird und damit im Register ganz forne ist.
        WriteData(li);
        StoreDataRegister();
        RegisterStep = 1;

      }else if (RegisterStep < 8) {

        String o = String("RegisterStep " + String(RegisterStep));
        Serial.print(o);
        RegisterStep = RegisterStep + 1;

      }else if(RegisterStep == 8) {
        String o = String("RegisterStep " + String(RegisterStep));
        Serial.print(o);

        bool li[] = {true, true,true,false,false,true,true,true};   //Betrug, der Arduino schreibt alles auf einmal und nicht schritt für schritt :)
        WriteData(li);
        StoreDataRegister();
        RegisterStep = RegisterStep + 1;

      }else if(RegisterStep < 14) {
        String o = String("RegisterStep " + String(RegisterStep));
        Serial.print(o);

        RegisterStep = RegisterStep + 1;

      }else if(RegisterStep == 14) {
        String o = String("RegisterStep " + String(RegisterStep));
        Serial.print(o);

        bool li[] = {true, true,true, false,true,false,true,false};   //Betrug, der Arduino schreibt alles auf einmal und nicht schritt für schritt :)
        WriteData(li);
        StoreDataRegister();
        RegisterStep = RegisterStep + 1;

      }else if (RegisterStep < 16) {
        String o = String("RegisterStep " + String(RegisterStep));
        Serial.print(o);

        RegisterStep = RegisterStep + 1;

      }else if (RegisterStep < 28 && RegisterStep%2 == 0) {   // bei geraden Step

        String o = String("RegisterStep " + String(RegisterStep));
        Serial.print(o);
        bool li[] = {true,false,true,false,true,false,true,false}; 
           //Betrug, der Arduino schreibt alles auf einmal und nicht schritt für schritt :)
        WriteData(li);
        StoreDataRegister();
        RegisterStep = RegisterStep + 1;

      }else if (RegisterStep < 28 && RegisterStep%2 == 1) {             // bei ungeradem Step  
        
        String o = String("RegisterStep " + String(RegisterStep));
        Serial.print(o);
        bool li[] = {false, true,false,true,false,true,false,true};
          
        WriteData(li);
        StoreDataRegister();
        RegisterStep = RegisterStep + 1;

      }else if (RegisterStep <35 ) {

        String o = String("RegisterStep " + String(RegisterStep));
        Serial.print(o);
        RegisterStep = RegisterStep + 1;

      }else if (RegisterStep == 35) {

        String o = String("RegisterStep " + String(RegisterStep));
        Serial.print(o);
        RegisterStep = 0;
      }
    
      lastTimeRegister = millis ();
    }
    


  }else if (ShiftRegisterConnected == false && digitalRead(Register5V) == false) {

    pinMode(Register5V, OUTPUT);
    digitalWrite(Register5V, HIGH);
    digitalWrite(MasterReset, LOW);
    delay(1);
    digitalWrite(MasterReset, HIGH);


    ShiftRegisterConnected = true;


    RegisterStep = 0;
    RegisterSmallStep = 0;

  }

  
}





void SerialCeck() {

  if (Serial.available() > 0) {
    //int command = Serial.read();

  }
}

void checkButton() {
  if (digitalRead(BUTTON_PIN) == false && ButtonPressed == false) {
    Serial.print("Button");
    ButtonPressed = true;
  }else if(digitalRead(BUTTON_PIN) == true && ButtonPressed == true) {
    ButtonPressed = false;
  }
}

void CheckLED() {

  // check for connected LED and switch to output

  if (LEDConnected == true) {

    pinMode(LED_PIN, INPUT_PULLUP);
    if (digitalRead(LED_PIN) == true) {
        LEDConnected = false;
        Serial.print("LEDDisonnected");
    }else {
      pinMode(LED_PIN, OUTPUT);
    }

  }else if (LEDConnected == false && digitalRead(LED_PIN) == false) {
    
    pinMode(LED_PIN, OUTPUT);
    digitalWrite(LED_PIN, HIGH);
    LEDConnected = true;        //add to reset
    Serial.print("LEDConnected");
  }

  

}

void CheckRGBLED() {



  if (RGBConnected == true) {

    pinMode(G_PIN, INPUT_PULLUP);
    if (digitalRead(G_PIN) == true) {
      RGBConnected = false;
      Serial.print("RGBDisconnected");
      pinMode(R_PIN, INPUT_PULLUP);
      pinMode(B_PIN, INPUT_PULLUP);

    }else {
      pinMode(G_PIN, OUTPUT);
    }


  }else if (RGBConnected == false && digitalRead(G_PIN) == false ) { //&& digitalRead(G_PIN) == false && digitalRead(B_PIN) == false
    pinMode(R_PIN, OUTPUT);
    pinMode(G_PIN, OUTPUT);
    pinMode(B_PIN, OUTPUT);

    digitalWrite(R_PIN, HIGH);
    digitalWrite(G_PIN, HIGH);
    digitalWrite(B_PIN, HIGH);
  
    RGBConnected = true;        //add to reset
    Serial.print("RGBConnected");


  }

}

void WiderstandMessen() {

  

  float messuredVoltage = analogRead(RES_PIN);
    if (int(messuredVoltage) <= 1000) {

      //Serial.print("AnalogValue: ");
      //Serial.println(messuredVoltage);


      float buffer= messuredVoltage * 5;
      float Vout= (buffer)/1024.0;
      //Serial.print("Vout: ");
      //Serial.println(Vout);
      buffer= (5-Vout)/Vout;
      float R2 = 5100 / buffer;
      String o = String("RES " + String(R2));
      Serial.print(o);

  }

  

}

void Debugger() {

  Serial.print("MR: ");
  Serial.print(digitalRead(MasterReset));
  Serial.print(" SchiftClock: ");
  Serial.print(digitalRead(ShiftClock));
  Serial.print(" Storage: ");
  Serial.print(digitalRead(StorageClock));
  Serial.print(" Data: ");
  Serial.println(digitalRead(Data));

}


void loop() {

  static uint32_t lasttime;

  CheckLED();
  delay(10);
  CheckRGBLED();
  delay(10);
  checkButton();
  delay(10);
  Schieberegister();
  delay(10);  // 10 ms delay, gegen Flut von Daten auf der Serial Verbindung

  uint32_t time = millis();

  if (time - lasttime > 200) {  //alle 200ms ausführen

    //Debugger();
    WiderstandMessen();
  }



}



