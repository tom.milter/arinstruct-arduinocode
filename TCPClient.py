#!/usr/bin/env python
import socket
import serial  #Serial Bibliotek fr Arduino
import time
import RPi.GPIO as GPIO  

GPIO.setmode(GPIO.BCM)

#TCP Configuration
host = '192.168.43.165'		# Hier IP des Unity-Ausfuehrenden Handys angeben
host2 = '192.168.43.230'
port = 5560

so = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
try:
	so.connect((host2, port))			#try connect to Handy
except:
	try:
		so.connect((host, port))			# try connect to PC
	except:
		print("no connection")
		exit()

#Serial configuration
se = serial.Serial('/dev/ttyUSB0',9600)
time.sleep(5)
print("se opend")
se.flushInput()

GPIO.setup(17, GPIO.IN, pull_up_down = GPIO.PUD_UP)
GPIO.setup(27, GPIO.IN, pull_up_down = GPIO.PUD_UP)
GPIO.setup(22, GPIO.IN, pull_up_down = GPIO.PUD_UP)

def checkSerial():
	if se.inWaiting()>0:
		response = se.read(se.inWaiting())
		print(response.decode('utf-8'))
		command = "SERIAL " + str(response.decode('utf-8'))
		sendTCP(command)



def checkButtons():

	global UpPressed
	global DownPressed
	global EnterPressed
	global RestartPressed
	global t
	if(GPIO.input(22) == False and UpPressed == False):
		command = "Bup"
		print("Bup")
		sendTCP(command)
		UpPressed = True
		
		t = int(round(time.time() * 1000))			#time in ms
	if (UpPressed == True and GPIO.input(22) == True and int(round(time.time() * 1000))-t >= 250):		#250 ms pause
		UpPressed = False

	if(GPIO.input(27)== False and DownPressed == False ):
		command = "Bdown"
		print("Bdown")
		sendTCP(command)
		DownPressed = True

		t = int(round(time.time() * 1000))
	if (DownPressed == True and GPIO.input(27) == True and int(round(time.time() * 1000))-t >= 250):
		DownPressed = False

	if(GPIO.input(17)== False and EnterPressed == False):
		command = "Benter"
		print("Benter")
		sendTCP(command)
		EnterPressed = True		

		t = int(round(time.time() * 1000))
	if(GPIO.input(22)== False and GPIO.input(27)== False and RestartPressed == False):
		so.close
		so.connect((host,port))
		RestartPressed = True

		t = int(round(time.time() * 1000))
	if((EnterPressed == True or RestartPressed == True) and GPIO.input(17) == True and int(round(time.time() * 1000))-t >= 250):
		EnterPressed = False
		RestartPressed = False



#UnityBefehle:
#
# Bup
# Bdown
# Benter
# SERIAL
#	RES "Widerstand"
#	Button
#	LEDConnected
#	LEDDisconnected
#	RGBConnected
#	RGBDisconnected
#
UpPressed = False
DownPressed = False
EnterPressed = False
RestartPressed = False
t = None

def sendTCP(command):
	so.send(str.encode(command))






def checkTCP():
	reply = s.recv(1024)
	print(reply.decode('utf-8'))



def close():
	so.close()
	se.close()


#main LOOP

try:
	while True:
		checkSerial()
		checkButtons()
	#	checkTCP()
except KeyboardInterrupt:
	close()
